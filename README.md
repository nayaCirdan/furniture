# STEP PROJECT ONLINE SHOP "BAZAR"

[Project at GitLab Pages](https://nayacirdan.gitlab.io/furniture)

## Students:
- Student 1: Oksana Kirdan
- Student 2: Andrew Somin

## What we used:
- Bootstrap;
- Gulp;
- JS;
- jQuery;
- Owl Carousel 2;
- SCSS;
- BEM-naming;
- Font Awesome;
***

**Student 1**
- Navbar;
- Header;
- Top-menu;
- Top-slider;
- Our Features;
- Furniture Gallery.
- Modals for Login, Register, Cart Full, Cart Empty

**Student 2** 
- Stocks;
- Furniture;
- Filters;
- Blog;

### ATTENTION!!!
Use commands 'gulp build' and 'gulp dev'

For building in fast mode in gulpfile should be isDev = true;

isDev = false will run advanced mode.

By defauld cart has 1 item.

Type '0' instead '1' to use modal for empty cart;